package com.developers;

public class TesteConta {

	public static void main(String[] args) {
		
		Conta contaCorrente = new Conta();
		Conta contaPoupanca = new Conta();
		Conta contaInvestimento = new Conta();
		
		contaCorrente.numero = 11;
		contaCorrente.saldo = 20.50;
		contaCorrente.clinte.nome = "André Ribeiro";
		contaCorrente.clinte.idade = 37;
		
		contaPoupanca.numero =22;
		contaPoupanca.saldo = 30.30;
		contaPoupanca.clinte.nome = "Marcello Gallardo";
		contaPoupanca.clinte.idade = 37;
		
		contaInvestimento.numero = 01;
		contaInvestimento.saldo = 84.91;
		contaInvestimento.clinte.nome = "Charlles Babage";
		contaInvestimento.clinte.idade = 37;
		
		System.out.println("Nome Cleinte: " + contaCorrente.clinte.nome);
		System.out.println("Idade do Cliente: " + contaCorrente.clinte.idade);
		System.out.println("Numero da conta corrente: " + contaCorrente.numero);
		System.out.println("Saldo em conta: " + contaCorrente.saldo);
		System.out.println("*****************************************************");
		
		System.out.println("Nome Cleinte: " + contaPoupanca.clinte.nome);
		System.out.println("Idade do Cliente: " + contaPoupanca.clinte.idade);
		System.out.println("Numero da conta corrente: " + contaPoupanca.numero);
		System.out.println("Saldo em conta: " + contaPoupanca.saldo);
		System.out.println("*****************************************************");
		
		System.out.println("Nome Cleinte: " + contaInvestimento.clinte.nome);
		System.out.println("Idade do Cliente: " + contaInvestimento.clinte.idade);
		System.out.println("Numero da conta corrente: " + contaInvestimento.numero);
		System.out.println("Saldo em conta: " + contaInvestimento.saldo);
	
	}
}